FROM blenderfox/xenial-base

EXPOSE 80

RUN echo "deb http://gb.archive.ubuntu.com/ubuntu/ xenial main restricted universe multiverse" > /etc/apt/sources.list
RUN echo "deb-src http://gb.archive.ubuntu.com/ubuntu/ xenial main restricted universe multiverse" >> /etc/apt/sources.list
RUN apt-get update;apt-get -y install nginx php php-cli php-cgi php-fpm vim curl screen

#Edit php.ini

RUN sed -i s/';cgi.fix_pathinfo=1'/'cgi.fix_pathinfo=1'/g /etc/php/7.0/fpm/php.ini && sudo service php7.0-fpm restart

COPY scripts/phpinfo.php /var/www/html/
COPY index.php /var/www/html/index.php
COPY headers.php /var/www/html/headers.php
COPY post.php /var/www/html/post.php
COPY handler.sh /usr/local/bin/handler.sh
COPY sites-available-default /etc/nginx/sites-available/default

COPY startup.sh /usr/local/bin/startup.sh

RUN chmod +x /usr/local/bin/*.sh
RUN rm -v /var/www/html/index.nginx-debian.html

CMD ["bash","/usr/local/bin/startup.sh"]
