# webhook-bash-processor

This repo contains the basic skeleton of an environment that lets you invoke an external script (e.g. Bash) upon a request. This is useful for listening to webhooks such as from Slack and for responding to commands from within there.

Use PHP to identify the information you want to pass to your external script and parse the information from within your script.

For example:

```
<?php
    $output = shell_exec('/usr/local/bin/slackhandler.sh \'' . $_SERVER["QUERY_STRING"] . '\'');
    echo $output;
?>
```

Will pass the entire GET query string to the handler

Optionally, you can bundle the screen command onto the handler if the turnaround is too slow

```
<?php
    $output = shell_exec('screen -dmS slackhandler /usr/local/bin/slackhandler.sh \'' . $_SERVER["QUERY_STRING"] . '\'');
    echo $output;
?>
```
